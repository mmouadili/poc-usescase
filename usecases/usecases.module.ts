import { Module,HttpModule } from '@nestjs/common';
import { UsecasesController } from './usecases.controller';
import { UsecasesService } from './usecases.service';

@Module({
  imports: [HttpModule
  ],
  controllers: [UsecasesController],
  providers: [UsecasesService]
})
export class UsecasesModule {}
