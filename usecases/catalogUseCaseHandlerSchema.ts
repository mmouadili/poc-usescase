import { IsNotEmpty,ValidateIf } from 'class-validator';
export class catalogUseCaseHandlerSchema {
//Schema used in the handler to properly display the information to the front end.
@IsNotEmpty()
ucid : string; 
@ValidateIf(o => !o.title)
title : string; 
product :string; 
service : string; 
mitre_tactics :[];
mitre_techniques :[];
comment ? : string; 
customer_already_have_it :BigInteger
description ?: string; 
difficulty ? : string; 
goal ?: string; 
logging_requirements: string; 
response_severity ?: string; 
response_false_positives ? : string; 

}