import { Controller,Get,Param,Query,Post} from '@nestjs/common';
import { UsecasesService } from './usecases.service';
import{ ApiProperty,ApiQuery} from '@nestjs/swagger'


@Controller('usecases')
export class UsecasesController {
    constructor(private useCaseService :UsecasesService  ){}

    // @Get()
    // @ApiQuery({name: 'filters',required :false, schema:{
    //     type:'object'
    // }})

    // @ApiQuery({name: 'sort', required : false,schema:{
    //     type:'string'
    // }})

    //  getAllCatalog(@Query() queryFilters?,@Query('sort') querySort ?:string){
    //     return this.useCaseService.getCatalogUseCases(queryFilters,querySort);
       
    // }
    @Get('usecases-catalog')
    
     getAllCatalogyml(@Query() queryFilters?,@Query('sort') querySort ?:string){
        return this.useCaseService.get_usecases_catalog();
       
    }
    @Get('customer-usecases/:code')
    
     getUseCaseCustomer(@Param('code') code: string){
         if (this.useCaseService.usecases_definitions_custom_new[code]){
            return this.useCaseService.get_customer_usecases(code)

         }else{
            return "Customer " + code + " is invalid"
         }
     
       
    }
    @Post('use_case/:code/templateToHtml')
    
     getUseCaseTemplate(@Query() queryFilters,@Param('code') code: string){
        // if (this.useCaseService.usecases_definitions_custom_new[code]){
            return this.useCaseService.templatetoHtml(code,queryFilters)

        //  }else{
        //     return "Customer " + code + " is invalid"
        //  }
     
       
    }
    @Get(':code')
    @ApiQuery({name: 'filters',required :false, schema:{
        type:'object'
    }})

    @ApiQuery({name: 'sort', required : false,schema:{
        type:'string'
    }})
    getUseCasesCustomer(@Param('code') code: string,@Query() queryFilters,@Query('sort') querySort ?:string){
        return this.useCaseService.getCustomerUseCases(code,queryFilters,querySort);
    }

    @Get('repartitionstatus/:code')
    getUseCasesCustomerStatusRepatition(@Param('code') code: string){
        return this.useCaseService.getCustomerUsecaseRepartitionStatus(code);
    }
    @Post('issues')
    @ApiQuery({name: 'code', required : false,schema:{
        type:'string'
    }})
    @ApiQuery({name: 'issueId', required : false,schema:{
        type:'string'
    }})
    @ApiQuery({name: 'body', schema:{
        type:'object'
    }})
    createUseCaseIssue(@Query('code') code: string,@Query() body,@Query('issueId') issue ?:string){
        return 'ok';
    }
    
}
