import { Test, TestingModule } from '@nestjs/testing';
import { UsecasesController } from './usecases.controller';

describe('UsecasesController', () => {
  let controller: UsecasesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsecasesController],
    }).compile();

    controller = module.get<UsecasesController>(UsecasesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
