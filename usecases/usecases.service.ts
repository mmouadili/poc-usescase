import { Injectable,Query, HttpService,InternalServerErrorException} from '@nestjs/common';
import { AxiosResponse } from 'axios'
import { catalogUseCaseHandlerSchema } from './catalogUseCaseHandlerSchema';
import { Observable } from 'rxjs';
import * as https from 'https';
import { map } from 'rxjs';
import { readFileSync } from 'fs';
import * as yaml from 'js-yaml';
import { join } from 'path';
import * as path from 'path';


 
import {
    validate,
    validateOrReject
  } from 'class-validator';
import {ConfigService} from '@nestjs/config'
import { inherits } from 'util';
import { fileURLToPath } from 'url';
@Injectable()

export class UsecasesService {
requiredField : string[] = ["goal"];
 ALERTING_PROJECTS_PATH : string;
 ALERTING_CONFIG_PATH : string;
 ALERTING_USECASE_PATH : string;
 ALERTING_DEPRECATED_USECASE_PATH : string;
 usecases_definitions_new : object = {};
 usecases_template_new :object[];
 usecases_allFiles_new :object[];

 usecases_definitions_custom_new : object = {}
 customer_usecases_new : object = {}
 raw_usecases_data_new : object = {}
 USECASE_DOC_FILE_NAME :string;
 USECASE_CONFIG_FILE_NAME :string;
 IGNORE_YML_PATH_SUFFIXES : string [];
 ACCES_TOKEN : string;

constructor( private httpService : HttpService, private configService : ConfigService
  
){
  this.init();
};
public init = async function init (){
  this.ALERTING_PROJECTS_PATH = (this.configService.get('usecase_api_url'))?this.configService.get('usecase_api_url') : '/home/merouane/usecase_api/poc-usecase-api/usecases-api/src';
  this.ALERTING_CONFIG_PATH = this.ALERTING_PROJECTS_PATH + "alerting-config/customers";
  //this.ALERTING_USECASE_PATH = this.ALERTING_PROJECTS_PATH + "alerting-usecase/use-cases";
  this.ALERTING_USECASE_PATH = this.ALERTING_PROJECTS_PATH + "alerting-usecase/use-cases";
  this.ALERTING_DEPRECATED_USECASE_PATH = this.ALERTING_PROJECTS_PATH + "alerting-usecase/Deprecated usecases";
  this.usecases_definitions_new = {}
  this.usecases_template_new = [];
  this.usecases_definitions_custom_new  = {}
  this.customer_usecases_new = {}
  this.raw_usecases_data_new = {}
  this.USECASE_DOC_FILE_NAME = 'documentation.yml';
  this.USECASE_CONFIG_FILE_NAME = 'config.yml';
  this.IGNORE_YML_PATH_SUFFIXES = [
    "-lognorm.yml",
    "_spl.yml",
    "_kql.yml"
  ]
  this.ACCES_TOKEN = this.configService.get('acces_token')
  var files = [],filesDeprecated= [],customerFiles = [];
  var serverUrl = this.configService.get('usecase_api_url')
  var serverUrlCustomer = this.configService.get('usecase_api_customer')

  const allfilesUsecases = await this.getFilesFromRepository(serverUrl+'/tree?recursive=false&path=use-cases/Endpoint&page=1&per_page=100&access_token='+this.ACCES_TOKEN,files,false)
   const ymlFilesUseCase = allfilesUsecases.filter(el => (el["name"].indexOf('.yml')!==-1|| el["name"].indexOf('.yaml') !== -1 )&& !this.isIgnoreFile(el["name"]))
  // const allfilesUcDeprecated = await this.getFilesFromRepository('https://gitlab.redsen.hk.local/api/v4/projects/10/repository/tree?recursive=false&path=Deprecated%20usecases&page=1&per_page=100&access_token=jpBdbmBoum7LoxGryyax',filesDeprecated,false)
  // const ymlFilesUseCaseDep = allfilesUcDeprecated.filter(el => (el["name"].indexOf('.yml')!==-1|| el["name"].indexOf('.yaml') !== -1 )&& !this.isIgnoreFile(el["name"]))
  const allfilesUcCustomers = await this.getFilesFromRepository(serverUrlCustomer+'/tree?recursive=false&path=customers&page=1&per_page=100&access_token='+this.ACCES_TOKEN,customerFiles,true)
  const ymlFilesUseCaseCustomers = allfilesUcCustomers.filter(el => (el["name"].indexOf(this.USECASE_CONFIG_FILE_NAME)!==-1));
  this.usecases_allFiles_new = allfilesUsecases
  const contentYmlFiles = await this.getcontentFiles(ymlFilesUseCase,true,false);
 // const contentYmlDepFiles = await this.getcontentFiles(ymlFilesUseCaseDep,true,false);
  const contentYmlCustomersFiles = await this.getcontentFiles(ymlFilesUseCaseCustomers,true,true);


//   this.getUseCaseCatalog(allfilesUsecases,ymlFiles,false).then(files => {
//     this.getUseCaseCatalog(this.ALERTING_DEPRECATED_USECASE_PATH,true).then(result=>{

//       return this.getUseCaseCustomer ()
//      });
// });
this.getUseCaseCatalog(allfilesUsecases,contentYmlFiles,false)
//this.getUseCaseCatalog(allfilesUcDeprecated,contentYmlDepFiles,true)
 this.getUseCaseCustomer (contentYmlCustomersFiles)
}
 isIgnoreFile(fileName) {
  var isIgnoreFile = false;
  this.IGNORE_YML_PATH_SUFFIXES.forEach(element => {
    if (fileName.indexOf(element, fileName.length - element.length) !== -1){
      isIgnoreFile = true
    }
  });
  return  isIgnoreFile;
}
get_usecases_catalog(){
  var result = {};
  for (var key in this.usecases_definitions_new) {
    if (!this.usecases_definitions_new[key]["deprecated"]){
      result[key] = this.usecases_definitions_new[key]
    }

  }
  return result;
  
}
get_customer_usecases(code : string){
  var result = {};
  var customer_ucs = this.createCopy(this.usecases_definitions_custom_new[code]);
  for (var key in customer_ucs) {
    if (customer_ucs[key]["deprecated"]){
     delete customer_ucs[key]["deprecated"] 
    }
    result[key] = customer_ucs[key]

  }
  return result;

}
async getTodos(todoUrl: string): Promise <Observable<Array<object>>>{
    return this.httpService.get(todoUrl).pipe(
      map(resp => resp.data),
    );
  }


async getCatalogUseCases(@Query() query,querySortBy? : string):Promise<AxiosResponse>{
    
    const Url = this.configService.get('usecase_api_url')+'usecases-catalog'
        
    const response = await this.httpService.get(Url).toPromise().catch( e => {
        console.log( e );
        throw new InternalServerErrorException( e );
    } );
    const usecasesCatalog : catalogUseCaseHandlerSchema[] = [];
    const keys = ['count', 'page', 'size','sort'];
    var sortBy = [];
     // apply filter 
     for(var k in query) {
        if (!keys.includes(k)){
            if (query[k].indexOf(',') > -1){
                var partsOfvalues = query[k].split(',');
                var resultUseCase = [];
                partsOfvalues.forEach((element,index) => {
                    var res= this.getFilteredDatas(response.data, k, element);
                    resultUseCase = resultUseCase.concat(res)
                    
                });
                 
                response.data = resultUseCase;
            }else{
                response.data = this.getFilteredDatas(response.data, k, query[k]);
            }
        }

      } 
      if (querySortBy){
        if (querySortBy.indexOf(',') > -1){
            const partsOfvalues :any = querySortBy.split(',');
            sortBy = sortBy.concat(partsOfvalues)
        }
        else
        {
            sortBy.push(querySortBy)
        }
    }
      //sorted data
      response.data = response.data.sort(this.getsortedDatas(sortBy))
      response.data.forEach(elm => {
         this.checkValidateUseCaseCatalog(elm);
        const objUseCaseCatalog = new catalogUseCaseHandlerSchema(); 
       
        for(var k in elm) {
          //  if (objUseCaseCatalog.hasOwnProperty(k)) {
                objUseCaseCatalog[k]=elm[k];
           // }
        } 
        usecasesCatalog.push(objUseCaseCatalog)
      })
      const result = JSON.parse(JSON.stringify(usecasesCatalog))
     return result;
    
      //return result;

    
      
}

async getFilesFromRepository(url : string,files, forCustomer :boolean):Promise<any>{
  
  const agent = new https.Agent({  
    rejectUnauthorized: false
   });
 // var response = await this.httpService.get(url,{httpsAgent: agent}).toPromise();
 
  const response = await this.httpService.get(url,{httpsAgent: agent}).toPromise().catch( e => {
    console.log( e );
    throw new InternalServerErrorException( e );
} );
for( var folder in response.data ){
  if(response.data[folder]['type'] =='tree'){
   if (forCustomer) 
var urlsubFolder = 'https://gitlab.redsen.hk.local/api/v4/projects/9/repository/tree?recursive=false&path='+response.data[folder]['path']+'&page=1&per_page=100&access_token=jpBdbmBoum7LoxGryyax'
else var urlsubFolder = 'https://gitlab.redsen.hk.local/api/v4/projects/10/repository/tree?recursive=false&path='+response.data[folder]['path']+'&page=1&per_page=100&access_token=jpBdbmBoum7LoxGryyax'

var result = await this.getFilesFromRepository(urlsubFolder,files,forCustomer);
// for( var elm in result.data ){
//   if(result.data[elm]['type'] =='tree'){
//     response.data.push(result.data[elm])
//   }else{
//     files.push(result.data[elm])
//   }
// }
  }else{
    files.push(response.data[folder])
  }
}

  return new Promise((resolve, reject) => {

    try {
  

var t = response 
return resolve(files)

} catch (e) {
  console.log(e);
}
});
}
async getcontentFiles(files,isYmlFiles,forCustomer):Promise<any>{
  var fileContents = [];
  const yamlToJson = (content) => yaml.load(Buffer.from(content, 'base64').toString());
  const agent = new https.Agent({  
    rejectUnauthorized: false
   });
   if (forCustomer)
 var urlUseCases = 'https://gitlab.redsen.hk.local/api/v4/projects/9/repository/' +'files/'
 else  var urlUseCases = 'https://gitlab.redsen.hk.local/api/v4/projects/10/repository/' +'files/'

for( var file in files ){
  var urlfolder = files[file]['path'].replace(/\//g,'%2F');
  urlfolder = urlfolder.replace(/\ /g,'%20');
  var url = urlUseCases +urlfolder+'?ref=master&access_token=stNzoxb-EDG5ziPGkmk8'
    
  const response = await this.httpService.get(url,{httpsAgent: agent}).toPromise().catch( e => {
    console.log( e );
    throw new InternalServerErrorException( e );
} );
if (isYmlFiles){
  
  var ymlContent = yamlToJson(response.data['content'])
  response.data['ymlContent'] = ymlContent
}
fileContents.push(response.data);
}

  return new Promise((resolve, reject) => {

    try {
return resolve(fileContents)

} catch (e) {
  console.log(e);
}
});
}
async templatetoHtml(codeUc: string, queryParam : Object){
   // template file 
   var templateDoc = null,template = {},usecases_template_new= [];
  
     const templateName = codeUc+'_portal_template.j2'
     const ymlTemplateFile = this.usecases_allFiles_new.filter(el => el["name"].indexOf(templateName)!==-1)
     if (ymlTemplateFile && ymlTemplateFile.length >0 ){
      var urlfolder = ymlTemplateFile[0]['path'].replace(/\//g,'%2F');
      urlfolder = urlfolder.replace(/\ /g,'%20');
      var urlUseCases = 'https://gitlab.redsen.hk.local/api/v4/projects/10/repository/' +'files/'
      var url = urlUseCases +urlfolder+'?ref=master&access_token=stNzoxb-EDG5ziPGkmk8'
      const agent = new https.Agent({  
        rejectUnauthorized: false
       });
      const response = await this.httpService.get(url,{httpsAgent: agent}).toPromise().catch( e => {
        console.log( e );
        throw new InternalServerErrorException( e );
    } );
    var templateUs = Buffer.from(response.data.content, 'base64').toString();
    for(var i in queryParam){
      if (i != 'custom_fields'){
        var searchRegExp  = '{'+i+'}'
        templateUs = templateUs.split(searchRegExp).join(queryParam[i]);
      }else{
        var customFields = JSON.stringify(queryParam[i])
        var obj = JSON.parse(customFields);
        for ( var elm in obj){
          var searchRegExp  = '{'+elm+'}'
          templateUs = templateUs.split(searchRegExp).join(obj[elm]);
        }
      }
      

    }
    return templateUs

     //templateDoc = readFileSync(dirname+'/'+ ymlTemplateFile[0], 'utf-8');
      // const file = readFileSync('https://raw.githubusercontent.com/mitre/cti/master/enterprise-attack/enterprise-attack.json', 'utf-8');

     
    //  template['usecase'] = filename.replace('.yml','')
    //  template['template'] = templateDoc;
    //  usecases_template_new.push(template);
     }
  

}

 groupBystatus(array, key) {
    // Return the end result
    return array.reduce((result, currentValue) => {
      // If an array already present for key, push it to the array. Else create an array and push the object
      (result[currentValue[key]] = result[currentValue[key]] || []).push(
        currentValue
      );
      // Return the current iteration `result` value, this will be taken as next iteration `result` value and accumulate
      return result;
    }, {}); // empty object is the initial value for result object
  };

  getsortedDatas(sortValues:Array<string>) {
    
    var props = sortValues;
    function dynamicSort(property) {
        var sortOrder = 1;
        if(property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function (a,b) {
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        }
    }
    return function (obj1, obj2) {
        var i = 0, result = 0, numberOfProperties = props.length;
        while(result === 0 && i < numberOfProperties) {
            result = dynamicSort(props[i])(obj1, obj2);
            i++;
        }
        return result;
    }
}
dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {

        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}
getFilteredDatas(array, key, value) {
    return array.filter(function(e) {
      return e[key] == value ;
    });
  }
  
checkValidateUseCaseCatalog (useCase : object){
   // if ()
}
async  validateOrRejectEntity(input):Promise<boolean> {
    try {
      await validateOrReject(input);
      return true;
    } catch (errors) {
      console.log('Caught promise rejection (validation failed). Errors: ', errors);
      return false;
    }
  }
 transformToPlain (catalog : object){
    const objUseCaseCatalog = new catalogUseCaseHandlerSchema();


 }
async getCustomerUseCases(code : string,@Query() query ?,querySortBy? : string):Promise<AxiosResponse>{
    var serverUsrl = this.configService.get('usecase_api_url')
    var url = serverUsrl +`customer-usecases/${code}`
    const response = await this.httpService.get(url).toPromise();
    const usecasesCatalog : catalogUseCaseHandlerSchema[] = [];
    const keys = ['count', 'page', 'size','sort'];
    var sortBy = [];
     // apply filter 
     for(var k in query) {
        if (!keys.includes(k)){
            if (query[k].indexOf(',') > -1){
                var partsOfvalues = query[k].split(',');
                var resultUseCase = [];
                partsOfvalues.forEach((element,index) => {
                    var res= this.getFilteredDatas(response.data, k, element);
                    resultUseCase = resultUseCase.concat(res)
                    
                });
                 
                response.data = resultUseCase;
            }else{
                response.data = this.getFilteredDatas(response.data, k, query[k]);
            }
        }

      } 
      if (querySortBy){
        if (querySortBy.indexOf(',') > -1){
            const partsOfvalues :any = querySortBy.split(',');
            sortBy = sortBy.concat(partsOfvalues)
        }
        else
        {
            sortBy.push(querySortBy)
        }
    }
      //sorted data
      response.data = response.data.sort(this.getsortedDatas(sortBy))
     response.data.forEach(elm => {
         this.checkValidateUseCaseCatalog(elm);
        const objUseCaseCatalog = new catalogUseCaseHandlerSchema(); 
       
        for(var k in elm) {
          //  if (objUseCaseCatalog.hasOwnProperty(k)) {
                objUseCaseCatalog[k]=elm[k];
           // }
        } 
        usecasesCatalog.push(objUseCaseCatalog)
      })
      const result = JSON.parse(JSON.stringify(usecasesCatalog))
     return result;
       
}


// async readFiles(dirname) {
//   var fs = require('fs');
//   await fs.readdir(dirname, function(err, filenames) {
//     if (err) {
//      // onError(err);
//       return;
//     }
//     filenames.forEach(function(filename) {
//       fs.readFile(dirname + filename, 'utf-8', function(err, content) {
//         if (err) {
//         //  onError(err);
//           return;
//         }
//        // onFileContent(filename, content);
//       });
//     });
//   });
// }

//  const fs = require('fs');
//  const path = require('path');
 
  
 
   public getFolderDirectoris = function getDirectoriesRecursive(srcpath) {
    function flatten(lists) {
      return lists.reduce((a, b) => a.concat(b), []);
    }
     function getDirectories(srcpath) {
       const fs = require('fs');
       const path = require('path');
      return fs.readdirSync(srcpath)
        .map(file => path.join(srcpath, file))
        .filter(path => fs.statSync(path).isDirectory());
    }
   return [srcpath, ...flatten(getDirectories(srcpath).map(getDirectoriesRecursive))];
 }


readLocalFiles(dirname,forCustomer: boolean):Promise<any> {
  var fs = require('fs');
 function promiseAllP(items, block) {
    var promises = [];
    items.forEach(function(item,index) {
        promises.push( function(item,i) {
            return new Promise(function(resolve, reject) {
                return block.apply(this,[item,index,resolve,reject]);
            });
        }(item,index))
    });
    return Promise.all(promises);
  } 
  
function isIgnoreFile(fileName) {
  var isIgnoreFile = false;
  IGNORE_YML_PATH_SUFFIXES.forEach(element => {
    if (fileName.indexOf(element, fileName.length - element.length) !== -1){
      isIgnoreFile = true
    }
  });
  return  isIgnoreFile;
}
const USECASE_DOC_FILE_NAME = 'documentation.yml';
const USECASE_CONFIG_FILE_NAME = 'config.yml';

const IGNORE_YML_PATH_SUFFIXES = [
  this.USECASE_DOC_FILE_NAME,
  "-lognorm.yml",
  "_spl.yml",
  "_kql.yml"
]
  return new Promise((resolve, reject) => {
      fs.readdir(dirname, function(err, filenames) {
          if (err) return reject(err);
          var ymlFiles = null,ymlTemplateFiles = null,contentDoc = null;

          
          if (forCustomer){
             ymlFiles = filenames.filter(el => el.indexOf(USECASE_CONFIG_FILE_NAME)!==-1)

          }else {
          ymlFiles = filenames.filter(el =>!isIgnoreFile(el)&& (path.extname(el) === '.yml' || path.extname(el) === '.yaml'))
          const ymlDocmentationFile = filenames.filter(el => el.indexOf(USECASE_DOC_FILE_NAME)!==-1)
        
          var contentDoc = null
          if (ymlDocmentationFile && ymlDocmentationFile.length >0 ){
            contentDoc= yaml.load(readFileSync(dirname+'/'+ ymlDocmentationFile[0]), 'utf8');
          }
          }
          
          promiseAllP(ymlFiles,
          (filename,index,resolve,reject) =>  {
            if (!isIgnoreFile(filename)&&(filename.indexOf('.yml') !== -1 ||filename.indexOf('.yaml') !== -1 )){
              // template file 
              var templateDoc = null,template = {},usecases_template_new= [];
              if (!forCustomer){
                const templateName = filename.replace('.yml','')+'_portal_template.j2'
                const ymlTemplateFile = filenames.filter(el => el.indexOf(templateName)!==-1)
                if (ymlTemplateFile && ymlTemplateFile.length >0 ){
                templateDoc = readFileSync(dirname+'/'+ ymlTemplateFile[0], 'utf-8');
                 // const file = readFileSync('https://raw.githubusercontent.com/mitre/cti/master/enterprise-attack/enterprise-attack.json', 'utf-8');

                
                template['usecase'] = filename.replace('.yml','')
                template['template'] = templateDoc;
                usecases_template_new.push(template);
                }

              }
              const content  = yaml.load(readFileSync(dirname+'/'+ filename), 'utf8');
              return resolve({filename: filename, contents: content,path : dirname,contentDocumentation :contentDoc,usecases_template : usecases_template_new});
              // yaml.load(readFileSync(dirname, filename), 'utf-8', function(err, content) {
              //     if (err) return reject(err);
              //     return resolve({filename: filename, contents: content});
              // });
            }else return resolve ();
          })
          .then(results => {
              return resolve(results);
          })
          .catch(error => {
              return reject(error);
          });
      });
});

}


promiseAllP(items, block) {
  var promises = [];
  items.forEach(function(item,index) {
      promises.push( function(item,i) {
          return new Promise(function(resolve, reject) {
              return block.apply(this,[item,index,resolve,reject]);
          });
      }(item,index))
  });
  return Promise.all(promises);
} 

public getLocalUseCaseCustomer = function getUseCaseCustomeryml ():Promise<any>{
  return new Promise((resolve, reject) => {
  
  try {
  const folders  = this.getFolderDirectoris(this.ALERTING_CONFIG_PATH)
  const foldersNumber = folders.length;
  var folderpact = 0;
  folders.forEach( folder => {
      this.readFiles(folder,true)
      .then(files => {
        folderpact = folderpact +1 ;
        // transform  use case from yml to Json 
          console.log( "loaded ",files.length );
          
          files.forEach( (item, index) => {
            this.process_alerting_usecase_customers(item,this.usecases_definitions_new,this.usecases_definitions_custom_new,
              this.raw_usecases_data_new,
              this.ALERTING_CONFIG_PATH);
          });
          if (foldersNumber ==folderpact ) return resolve(this.usecases_definitions_new);
  
      })
      
      .catch( error => {
          console.log( error );
      });
  });
    
  } catch (e) {
    console.log(e);
  }
  });
  }
  public getUseCaseCustomer = function getUseCaseCustomeryml (files:any){
    try {    
           files.forEach( (elm, index) => {
            var item = {filename: elm['file_name'], contents:elm['ymlContent'],path : elm['file_path']}
              this.process_alerting_usecase_customers(item,this.usecases_definitions_new,this.usecases_definitions_custom_new,
                this.raw_usecases_data_new,
                this.ALERTING_CONFIG_PATH);
            });
    
    
      
    } catch (e) {
      console.log(e);
    }
    }
  process_alerting_usecase_customers(file,usecases_definitions_new,
    usecases_definitions_custom_new,
    raw_usecases_data_new,
    alerting_project_path) {
    // customer code 
   var customerCode = ''; 
    if (file && file.path){
      var partsOfvalues = file.path.split('/');
      customerCode = partsOfvalues[partsOfvalues.length-2]
    }
    usecases_definitions_custom_new[customerCode] = {};
    if (file['contents'] && file['contents']['usecases']) {
      for (var key in file['contents']['usecases']) {
        var uc_definition_clone = {}
        var usecase_config = file['contents']['usecases'][key];
       if (usecases_definitions_new[key]){
        uc_definition_clone =this.createCopy(usecases_definitions_new[key])
        this.update_usecase(uc_definition_clone,usecase_config)

        usecases_definitions_custom_new[customerCode][key] = uc_definition_clone;

       }
    }

    }
  }
  createCopy(objectToCopy: object): object{
    return (JSON.parse(JSON.stringify(objectToCopy)));
}
//implements use-case updatable-fields logic and mapping to the API fields
 update_usecase(usecase : object, usecase_config: object ){
    usecase["customer_already_have_it"] = true
    usecase["is_enabled_24_7"] = this.exec_ignore_errors(this.compute_is_enabled_24_7, usecase_config)
    usecase["siem"] = this.exec_ignore_errors(this.getSiem, usecase_config)
    usecase["status"] = usecase_config["status"]
    usecase["comment"] = usecase_config["comment"]
    usecase["ticket_id"] = usecase_config["ticket_id"]
 }
  


public getUseCaseCatalog =  async function getUseCaseyml (allfiles:object[],ymlFiles:object[], deprecated : boolean):Promise<any>{

try {
  const ymlUseCases  = ymlFiles.filter(el => el['file_name'].indexOf(this.USECASE_DOC_FILE_NAME) ==-1)        

  ymlUseCases.forEach( (elm, index) => {
  var filePath = elm['file_path'].replace(elm['file_name'],'')
  const ymlDocmentationFile = ymlFiles.filter(el => el['file_name'].indexOf(this.USECASE_DOC_FILE_NAME)!==-1 && el['file_path'].indexOf(filePath)!== -1)        
  var contentDoc = null
  if (ymlDocmentationFile && ymlDocmentationFile.length >0 ){
    contentDoc= ymlDocmentationFile[0]['ymlContent']
  }
  var item = {filename: elm['file_name'], contents:elm['ymlContent'],path : elm['file_path'],contentDocumentation :contentDoc,usecases_template : null}
          this.usecases_template_new = this.usecases_template_new.concat(item.usecases_template)
          this.process_alerting_usecase(item,this.usecases_definitions_new,this.usecases_definitions_custom_new,
            this.raw_usecases_data_new,
            this.ALERTING_USECASE_PATH, deprecated);
        });
  
} catch (e) {
  console.log(e);
}
}


compute_logsource(file){
    if (file["elastalert"]){
      if (file["elastalert"]["logsource"]){
        return file["elastalert"]["logsource"]
      }else{
        console.error("unknown backend, missing logsource mapping")
        return {}
      }
    } else if (file["sentinel"]){
      // FIXME decide/design a proper UC model..
      return {"product": "azure", "service": "sentinel"}
    }
        
    else{
      console.error("unknown backend, missing logsource mapping")
        return {}
    }
        

}


process_alerting_usecase(file,usecases_definitions_new,
  usecases_definitions_custom_new,
  raw_usecases_data_new,
  alerting_project_path, deprecated){
  if (this.is_valid_usecase(file.filename,file.contents)){
    // if( this.is_custom_usecase(file.contents)){
    //   usecases_definitions_custom_new[file.contents['usecase']] = file.contents["customers"]
    // }
    usecases_definitions_new[file.contents['usecase']] = this.usecase_definition_mapper(file.contents,file.contentDocumentation ,deprecated)
    
  }
  
}
usecase_definition_mapper (file,usecase_documentation: Object , deprecated: boolean) {

const logsource = this.compute_logsource(file)
const tags = file["tags"]

const result = {
"ucid":                     file["usecase"],
"title":                    file["title"],
"product":                  logsource.product,
"service":                  logsource.service,
"is_available_24_7":        this.exec_ignore_errors(this.compute_is_available_24_7, tags),
"response_severity":        this.exec_ignore_errors(this.compute_response_severity, file),
"goal":                     this.exec_ignore_errors(this.compute_goal, usecase_documentation),
"logging_requirements":     this.exec_ignore_errors(this.compute_logging_requirements, usecase_documentation),
"response_false_positives": this.exec_ignore_errors(this.compute_response_false_positives, usecase_documentation),
"mitre_tactics":            this.exec_ignore_errors(this.compute_mitre, tags),
"mitre_techniques":         this.exec_ignore_errors(this.compute_mitre, tags),
}
if (deprecated)
result["deprecated"] = true

return result

 }
 compute_is_available_24_7(usecase_tags: [string]): boolean{
 return usecase_tags["hk.possible_24_7"] ? true : false
 }
 compute_response_severity(file){
  const capitalize = function capitalize(word: string) {
    if (!word) return word;
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  }
  return capitalize(file["level"]);

 }
 compute_is_enabled_24_7(usecase_config: object){
    if (usecase_config["is_247"]){
      return usecase_config["is_247"]
    }else{
      return false;
    } 
 }
 getSiem(usecase_config: object){
  if (usecase_config["siem_backends"] && usecase_config["siem_backends"].length>0 ){
    return usecase_config["siem_backends"][0]
  }else{
    return null;
  } 
}

 compute_goal(fileDocumentation){
  const goal = (fileDocumentation && fileDocumentation["goal"] ) ? fileDocumentation["goal"] : null
  return goal;

 }
 compute_logging_requirements(fileDocumentation){
  const logging_requirements = (fileDocumentation && fileDocumentation["requirements"] &&fileDocumentation["requirements"]["logging_requirements"]  ) ? fileDocumentation["requirements"]["logging_requirements"] : null
  return logging_requirements;

 }
 compute_response_false_positives(fileDocumentation){
  const false_positives = (fileDocumentation && fileDocumentation["response"] &&fileDocumentation["response"]["false_positives"]  ) ? fileDocumentation["response"]["false_positives"] : null
  return false_positives;

 }
 compute_mitre (usecase_tags: [string]){
  return usecase_tags["compute_mitre"] ? true : false

 }

 


 is_valid_usecase(fileName,fileContent):boolean{
   if (!fileContent["usecase"]){
    console.error("Invalid usecase definition file: " + fileName)
    return false

   }else if (fileContent["tags"]&& fileContent["tags"].length >0){
     if (fileContent['tags'].indexOf('hk.cancelled')  >-1 || fileContent['tags'].indexOf('hk.internal') >-1){
      return false
     }else {
       return true
     }

   }else {
     return true;
   }

}
 exec_ignore_errors(function_to_run, args){
    
      return function_to_run(args)
 }
is_custom_usecase(fileContent):boolean{
   if (fileContent["customers"]){
    return true
   }else{
    return true;
   }
   
}


async getCustomerUsecaseRepartitionStatus(code : string):Promise<Array<string>>{
    var serverUsrl = this.configService.get('usecase_api_url')
    var url = serverUsrl +`/customer-usecases/${code}`
    const response = await this.httpService.get(url).toPromise();
    var groupedDatas = this.groupBystatus(response.data,'status')
    var result = []
    for ( var elm in  groupedDatas) {
        var data = {};
        data[elm] = groupedDatas[elm].length
        result.push(data)
    }

     return result;

}

}
