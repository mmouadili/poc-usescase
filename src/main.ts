import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import{DocumentBuilder, SwaggerModule} from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const option = new DocumentBuilder().setTitle('Use cases').setDescription('Use cases :catalog &&  Manage API').build();
  const document = SwaggerModule.createDocument(app,option);
  SwaggerModule.setup('api',app,document)
  await app.listen(7000);
  // const server = await app.listen(7000);
  // server.setTimeout(180);
 
}
bootstrap();
